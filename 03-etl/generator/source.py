import os
import json
import random
import sqlite3
import datetime
import argparse

from web3 import Web3

MIN_USERS      = os.environ.get('MIN_USERS',  50)
MAX_USERS      = os.environ.get('MAX_USERS', 100)

P_ACCRUAL      = os.environ.get('P_ACCRUAL',  0.50)
P_TRANSFER     = os.environ.get('P_ACCRUAL',  0.85)

MIN_ACCRUAL    = os.environ.get('MIN_ACCRUAL',    1)
MAX_ACCRUAL    = os.environ.get('MAX_ACCRUAL',  500)

MIN_ITERATIONS = os.environ.get('MIN_ITERATIONS',   500)
MAX_ITERATIONS = os.environ.get('MAX_ITERATIONS',  1000)
    
MIN_TIME_DELTA = os.environ.get('MIN_TIME_DELTA',      1)
MAX_TIME_DELTA = os.environ.get('MAX_TIME_DELTA',  86400)

TABLE_SCHEMAS = [
    'CREATE TABLE users (                      \
        _id      INTEGER NOT NULL PRIMARY KEY, \
        _address BINARY  NOT NULL              \
    );',

    'CREATE TABLE transfers (                                 \
        _id     INTEGER   NOT NULL PRIMARY KEY AUTOINCREMENT, \
        _from   INTEGER   NOT NULL,                           \
        _to     INTEGER   NOT NULL,                           \
        _amount INTEGER   NOT NULL,                           \
        _date   TIMESTAMP NOT NULL,                           \
                                                              \
        FOREIGN KEY (_to)   REFERENCES users(_id),            \
        FOREIGN KEY (_from) REFERENCES users(_id)             \
    );',

    'CREATE TABLE accruals (                                   \
        _id      INTEGER   NOT NULL PRIMARY KEY AUTOINCREMENT, \
        _to      INTEGER   NOT NULL,                           \
        _amount  INTEGER   NOT NULL,                           \
        _date    TIMESTAMP NOT NULL,                           \
                                                               \
        FOREIGN KEY (_to) REFERENCES users(_id)                \
    );'

]

def generate(size:int, iterations:int) -> dict:
    balances = [0, ] * size
    
    # Transactions
    accruals  = []
    transfers = []
    
    date = datetime.datetime.utcnow()
    
    for i in range(iterations):        
        date += datetime.timedelta(seconds = random.randint(MIN_TIME_DELTA, MAX_TIME_DELTA))
        
        if random.random() > P_ACCRUAL:
            to     = random.randint(0, size - 1)
            amount = random.randint(MIN_ACCRUAL, MAX_ACCRUAL) 
            
            accruals.append({'to': to, 'amount': amount, 'date': int(date.timestamp())})
            
            balances[to] += amount
        
        
        date += datetime.timedelta(seconds = random.randint(MIN_TIME_DELTA, MAX_TIME_DELTA))

        if random.random() > P_TRANSFER:
            # Only with balance >= 1.
            with_balance = [i for i in filter(lambda x:balances[x] >= 1, range(size))]
            
            if len(with_balance) == 0:
                continue
            
            from_ = random.choice(with_balance)
            # All except from_ user.
            to    = random.choice([i for i in filter(lambda x:x != from_, range(size))])
            
            amount = random.randint(1, balances[from_])
        
            transfers.append({'from': from_, 'to': to, 'amount': amount, 'date': int(date.timestamp())})
            
            balances[from_] -= amount
            balances[to]    += amount
    
    web3 = Web3()
  
    # Generate web3 accounts.
    users = [web3.eth.account.privateKeyToAccount(web3.keccak(os.urandom(32))) for i in range(size)]
    
    return {'users': users, 'balances': balances, 'accruals': accruals, 'transfers': transfers}

def export(size:int, users:list[str], accruals:list[dict], transfers:list[dict], db_path:str) -> None:
    if os.path.exists(db_path):
        raise FileExistsError()
        
    db = sqlite3.connect(db_path)
    
    for table_schema in TABLE_SCHEMAS:
        db.execute(table_schema)
    
    for tx in accruals:
        db.execute(f"INSERT INTO accruals (_to, _amount, _date) VALUES ({tx['to']}, {tx['amount']}, {tx['date']});")
    
    for tx in transfers:
        db.execute(f"INSERT INTO transfers (_from, _to, _amount, _date) VALUES ({tx['from']}, {tx['to']}, {tx['amount']}, {tx['date']});")
    
    for id, account in zip(range(size), users):
        db.execute(f"INSERT INTO users (_id, _address) VALUES ({id}, x'{account.address[2:]}');")
    
    db.commit()
    db.close()
    
def generate_and_export(db_path = 'data.db') -> dict:
    size       = random.randint(MIN_USERS, MAX_USERS)
    iterations = random.randint(MIN_ITERATIONS, MAX_ITERATIONS)
    
    data = generate(size, iterations)

    export(size, data['users'], data['accruals'], data['transfers'], db_path)
    
    return {'size': size, 'iterations': iterations, 'db_path': db_path, 'data': data}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-db', dest = 'db_path',    default = 'data.db', type = str,  help = 'Database path. Default is "data.db".')
    
    args = parser.parse_args()
    
    generate_and_export(args.db_path)
