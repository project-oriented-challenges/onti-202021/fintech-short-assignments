cp /data/input/student_response.txt /etl/solution.py

if [[ -v TRACEBACK ]]; then
    python -m pytest --force-testdox --disable-warnings --tb=${TRACEBACK}
else
    python -m pytest --force-testdox --disable-warnings
fi
 
