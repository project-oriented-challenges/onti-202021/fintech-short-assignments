import pytest

import os
import json
import string
import random
import subprocess

from web3 import Web3
    
from generator import generate_and_export
from utils     import send_transaction

@pytest.fixture(scope = 'session')
def web3():
    url = os.environ.get('WEB3_URL', 'https://sokol.poa.network')
    
    return Web3(Web3.HTTPProvider(url))

@pytest.fixture(scope = 'session')
def genesis(web3):
    return web3.eth.account.from_key(os.environ['PRIVATE_KEY'])

@pytest.fixture(scope = 'session')
def sample(tmpdir_factory):
    db_path = tmpdir_factory.mktemp('temp').join('data.db')
    
    return generate_and_export(str(db_path))

@pytest.fixture(scope = 'session')
def contract():
    return pytest.contract
    
@pytest.fixture(scope = 'function')
def account(sample):
    return random.choice(sample['data']['users'])

@pytest.fixture(scope = 'function')
def account_with_balance(web3, genesis, account):
    tx_receipt = send_transaction(web3, genesis, to = account.address, value = web3.toWei(0.05, 'ether'))
        
    assert tx_receipt['status'], tx_receipt["transactionHash"].hex()
    
    return account


def pytest_sessionstart(session):
    session.results = dict()


@pytest.hookimpl(tryfirst = True, hookwrapper = True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    result  = outcome.get_result()

    if result.when == 'call':
        item.session.results[item] = result

def pytest_sessionfinish(session):
    path = os.environ.get('JSON_REPORT_PATH')
    
    if not path:
        return
    
    json_report = {}
    
    if session.exitstatus == 0:
        json_report['score'] = 1
        json_report['msg']   = f'Meow, Done!\n'
        
    else:
        json_report['score'] = 0
        json_report['msg']   = f'Failed!\n'
    
    # Tests names.
    for (key, value) in session.results.items():
        index = value.nodeid.find('[')
        
        if index != -1:
            parameter = value.nodeid[index:]
        else:
            parameter = ''
        
        json_report['msg'] += f'{value.testdox_title}{parameter} -> {value.outcome}\n'
        
        try:
            json_report['msg'] += f'ERROR: {value.longrepr.reprcrash.message}\n\n'
        except:
            pass
        
    with open(path, 'w') as file:
        json.dump(json_report, file)
