import pytest

import os
import string
import random
import subprocess

from utils import send_transaction, call_function, deploy_contract

@pytest.mark.it('Test get bytecode and deploy.')
@pytest.mark.dependency(depends = [], name = 'deploy', scope = 'session')
def test_deploy(web3, genesis, sample):
    abi = [
        {
            "anonymous": False,
            "inputs": [
                {
                    "indexed": True,
                    "internalType": "address",
                    "name": "src",
                    "type": "address"
                },
                {
                    "indexed": True,
                    "internalType": "address",
                    "name": "guy",
                    "type": "address"
                },
                {
                    "indexed": False,
                    "internalType": "uint256",
                    "name": "wad",
                    "type": "uint256"
                }
            ],
            "name": "Approval",
            "type": "event"
        },
        {
            "anonymous": False,
            "inputs": [
                {
                    "indexed": True,
                    "internalType": "address",
                    "name": "src",
                    "type": "address"
                },
                {
                    "indexed": True,
                    "internalType": "address",
                    "name": "dst",
                    "type": "address"
                },
                {
                    "indexed": False,
                    "internalType": "uint256",
                    "name": "wad",
                    "type": "uint256"
                }
            ],
            "name": "Transfer",
            "type": "event"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "internalType": "address",
                    "name": "",
                    "type": "address"
                },
                {
                    "internalType": "address",
                    "name": "",
                    "type": "address"
                }
            ],
            "name": "allowance",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": False,
            "inputs": [
                {
                    "internalType": "address",
                    "name": "usr",
                    "type": "address"
                },
                {
                    "internalType": "uint256",
                    "name": "wad",
                    "type": "uint256"
                }
            ],
            "name": "approve",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": False,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [
                {
                    "internalType": "address",
                    "name": "",
                    "type": "address"
                }
            ],
            "name": "balanceOf",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": True,
            "inputs": [],
            "name": "totalSupply",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": False,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": False,
            "inputs": [
                {
                    "internalType": "address",
                    "name": "dst",
                    "type": "address"
                },
                {
                    "internalType": "uint256",
                    "name": "wad",
                    "type": "uint256"
                }
            ],
            "name": "transfer",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": False,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": False,
            "inputs": [
                {
                    "internalType": "address",
                    "name": "src",
                    "type": "address"
                },
                {
                    "internalType": "address",
                    "name": "dst",
                    "type": "address"
                },
                {
                    "internalType": "uint256",
                    "name": "wad",
                    "type": "uint256"
                }
            ],
            "name": "transferFrom",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": False,
            "stateMutability": "nonpayable",
            "type": "function"
        }
    ]
    
    args = ['python', os.environ['SOLUTION'], sample['db_path']]
    
    solution = subprocess.Popen(args, stdin  = subprocess.PIPE, 
                                      stdout = subprocess.PIPE, 
                                      stderr = subprocess.PIPE)
    
    try:
        (bytecode, errors) = solution.communicate(input   = sample['db_path'].encode(), 
                                                  timeout = os.environ.get('TIMEOUT', 10))
    
    finally:
        solution.kill()
     
    assert bytecode, 'The solution does not print anything.'
    
    bytecode = bytecode.decode().strip()
    
    assert set(bytecode).issubset(string.hexdigits), 'The bytecode must be a hex.'
    
    try:
        tx_receipt = deploy_contract(web3, genesis, abi, bytecode)
    except Exception as exc:
        raise Exception(f'Deploy error: {exc}')
    
    assert tx_receipt['status'], f'Deploy Failed: {tx_receipt.transactionHash.hex()}'
    
    pytest.contract = web3.eth.contract(address = tx_receipt.contractAddress, abi = abi)

@pytest.mark.it('Test predefined balances.')
@pytest.mark.dependency(depends = ['deploy'], name = 'balances', scope = 'session')
def test_balances(contract, sample):
    data = sample['data']
    
    for (account, balance) in zip(data['users'], data['balances']):
        try:
            balance_ = contract.functions.balanceOf(account.address).call()
        except Exception as exc:
            raise Exception(f'Cannot call balanceOf(): {exc}')
        
        assert balance == balance_, 'Balance mismatch.'


@pytest.mark.it('Test totalSupply() method.')  
@pytest.mark.dependency(depends = ['balances'], name = 'totalSupply', scope = 'session')
def test_totalSupply(contract):
    try:
        contract.functions.totalSupply().call()
    except Exception as exc:
        raise Exception(f'Cannot call totalSupply(): {exc}')

@pytest.mark.it('Test transfer() method and Transfer event. ')
@pytest.mark.dependency(depends = ['balances'], name = 'transfer', scope = 'session')
@pytest.mark.parametrize('amount_type', ['zero', 'overflow', 'random'])
def test_transfer(web3, contract, account_with_balance, account, amount_type):
    balance_sender   = contract.functions.balanceOf(account_with_balance.address).call()
    balance_receiver = contract.functions.balanceOf(account.address).call()
    
    if amount_type == 'zero':
        amount = 0
    
    elif amount_type == 'overflow':
        amount = balance_sender + 1
        
    elif amount_type == 'random':
        amount = random.randint(0, balance_sender)
    
    else:
        amount = int(amount_type)
    
    
    tx_receipt = call_function(web3, account_with_balance, contract.functions.transfer(account.address, amount), gas = 2 * 10**5)
    
    if amount_type == 'overflow':
        assert not tx_receipt['status'], f'Success transfer() {tx_receipt["transactionHash"].hex()}'
    
        return;
    
    assert tx_receipt['status'], f'Fail transfer() {tx_receipt["transactionHash"].hex()}'
    
    logs = contract.events.Transfer().processReceipt(tx_receipt)
    
    assert logs, f'No return data. {tx_receipt["transactionHash"].hex()}'
    
    args = logs[0]['args']
    
    assert args['src'] == account_with_balance.address,   'Sender account mismatch.'
    assert args['dst'] == account.address,                'Receiver account mismatch.'
    assert args['wad'] == amount,                         'Amount mismatch.'
    
    
    if account == account_with_balance:
        balance_sender   += amount
        balance_receiver -= amount
    
    balance_sender_   = contract.functions.balanceOf(account_with_balance.address).call()
    balance_receiver_ = contract.functions.balanceOf(account.address).call()
    
    assert balance_sender   - amount == balance_sender_,   'Sender balance mismatch.'
    assert balance_receiver + amount == balance_receiver_, 'Receiver balance mismatch.'

@pytest.mark.it('Test transferFrom(), approve(), allowance() methods and Approval event. ')
@pytest.mark.dependency(depends = ['balances'], name = 'transferFrom', scope = 'session')
@pytest.mark.parametrize('approval_type', ['zero', 'overflow', 'random'])    
def test_transferFrom(web3, genesis, contract, account_with_balance, account, approval_type):
    balance_sender   = contract.functions.balanceOf(account_with_balance.address).call()
    balance_receiver = contract.functions.balanceOf(account.address).call()
    
    if approval_type == 'zero':
        amount   = 0
        allowance = 0
    
    elif approval_type == 'overflow':
        allowance = random.randint(0, balance_sender)
        
        amount = allowance + 1
        
    elif approval_type == 'random':
        allowance = random.randint(0, balance_sender)
        
        amount = random.randint(0, allowance)
    
    else:
        allowance = int(approval_type)
        
        amount = random.randint(0, allowance)
    
    
    tx_receipt = call_function(web3, account_with_balance, contract.functions.approve(genesis.address, allowance), gas = 2 * 10**5)
    
    assert tx_receipt['status'], tx_receipt["transactionHash"].hex()
    
    logs = contract.events.Approval().processReceipt(tx_receipt)
    
    assert logs, f'No return data. {tx_receipt["transactionHash"].hex()}'
    
    args = logs[0]['args']
    
    assert args['src'] == account_with_balance.address, 'Sender account mismatch.'
    assert args['guy'] == genesis.address,              'Spender account mismatch.'
    assert args['wad'] == allowance,                    'Allowance mismatch.'
    
    try:
        allowance_ = contract.functions.allowance(account_with_balance.address, genesis.address).call()
    except Exception as exc:
        raise Exception(f'Cannot call allowance(): {exc}')
    
    assert allowance == allowance_, f'allowance({account_with_balance.address}, {genesis.address}) mismatch'
    
    
    tx_receipt = call_function(web3, genesis, contract.functions.transferFrom(account_with_balance.address, account.address, amount), gas = 2 * 10**5)
    
    if approval_type == 'overflow':
        assert not tx_receipt['status'], f'Success transferFrom() {tx_receipt["transactionHash"].hex()}'
        
        assert allowance == contract.functions.allowance(account_with_balance.address, genesis.address).call(), f'allowance({account_with_balance.address}, {genesis.address}) mismatch'
        
        return;
    
    assert tx_receipt['status'], f'Fail transferFrom() {tx_receipt["transactionHash"].hex()}'
    
    logs = contract.events.Transfer().processReceipt(tx_receipt)
    
    assert logs, f'No return data. {tx_receipt["transactionHash"].hex()}'
    
    args = logs[0]['args']
    
    assert args['src'] == account_with_balance.address,   'Sender account mismatch.'
    assert args['dst'] == account.address,                'Receiver account mismatch.'
    assert args['wad'] == amount,                         'Amount mismatch.'
    
    
    if account == account_with_balance:
        balance_sender   += amount
        balance_receiver -= amount
    
    balance_sender_   = contract.functions.balanceOf(account_with_balance.address).call()
    balance_receiver_ = contract.functions.balanceOf(account.address).call()
    
    assert balance_sender   - amount == balance_sender_,   'Sender balance mismatch.'
    assert balance_receiver + amount == balance_receiver_, 'Receiver balance mismatch.'
    
    assert allowance - amount == contract.functions.allowance(account_with_balance.address, genesis.address).call(), f'allowance({account_with_balance.address}, {genesis.address}) mismatch'
