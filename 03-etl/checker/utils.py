import os
import json
import requests

from web3     import Web3
from web3.eth import Account, Contract

GASPRICE_URL     = os.environ.get('GASPRICE_URL', 'https://gasprice.poa.network')
DEFAULT_GASPRICE = os.environ.get('DEFAULT_GASPRICE', 5.0) # in gwei

def get_gas_price() -> float:
    return DEFAULT_GASPRICE

def get_gas_price_from_oracle(type_ = 'fast') -> float:
     return json.loads(requests.get(GASPRICE_URL).text)[type_]
 
def send_transaction(web3:Web3, account:Account, **kwargs) -> dict:
    tx = {
        'from':     account.address,
        'nonce':    web3.eth.getTransactionCount(account.address),
        'gas':      4 * 10 ** 6,
        'gasPrice': int(get_gas_price() * web3.toWei('1', 'gwei'))
    }
    
    tx = kwargs.pop('tx', tx)
    
    tx.update(kwargs)
    
    signed = account.signTransaction(tx)
    
    tx_hash = web3.eth.sendRawTransaction(signed.rawTransaction)
    
    return web3.eth.waitForTransactionReceipt(tx_hash)

def call_function(web3:Web3, account:Account, function:Contract.functions, gas = 4 * 10 ** 6) -> dict:
    tx = function.buildTransaction({
        'from':     account.address,
        'nonce':    web3.eth.getTransactionCount(account.address),
        'gas':      gas,
        'gasPrice': int(get_gas_price() * web3.toWei('1', 'gwei'))
    })
    
    return send_transaction(web3, account, tx = tx)
    
def deploy_contract(web3:Web3, account:Account, abi:dict, bytecode:str) -> [Contract, None]:
    contract = web3.eth.contract(abi = abi, bytecode = bytecode)
    
    return call_function(web3, account, contract.constructor())
