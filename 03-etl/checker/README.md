Extract, Transform and Load (ETL)
===

[[_TOC_]]


## Репозиторий
```bash=
git clone https://gitlab.com/project-oriented-challenges/onti-202021/fintech-short-assignments.git
```


## Настройка файла `.env`
1. Перейдите в директорию `fintech-short-assignments/03-etl/checker`.
```bash=
cd fintech-short-assignments/03-etl/checker
```

2. Создайте файл `.env` через копирование файла `.env.example`.
```bash=
cp .env.example .env
```

3. Определите переменные `SOLUTION` и `PRIVATE_KEY`.
`TRACEBACK` - переменная от которой зависит размер traceback'а (влияет только на запуск внутри docker. Для запуска вне docker используйте аргумент --tb):

| Переменная  | Описание                                 |
| ------ | -------------------------------------------- |
| no     | Не отображать                          |
| native | Стандартный размер для python           |
| line   | Одна линия                    |
| short  | Короткий формат                    |
| long   | Расширенный формат |

`SOLUTION` - Путь до файла с решением. 

`PRIVATE_KEY` - Ключ для аккаунта со средствами.

```bash=
TRACEBACK=line

SOLUTION=/path/to/solution/file.py
PRIVATE_KEY=7bbf...52393
```


## Запуск вне docker
Для работы проекта необходим `python 3.9`.


1. Перейдите в директорию `fintech-short-assignments/03-etl/checker`.
```bash=
cd fintech-short-assignments/03-etl/checker
```

2. Создайте виртуальную среду.
```bash=
python -m venv env
```

3. Активируйте.
```bash=
source env/bin/activate
```

4. Установите `requirements.txt`.
```bash=
pip install -r -requirements.txt
```

5. Запустите.
```bash=
pytest --force-testdox --disable-warnings
```
или
```bash=
python -m pytest --force-testdox --disable-warnings
```


## Запуск внутри docker
1. Перейдите в директорию `fintech-short-assignments/03-etl`.
```bash=
cd fintech-short-assignments/03-etl
```

2. Сборка.
```bash=
sudo docker build -t ftchecker-etl:latest -f checker/Dockerfile .
```

3. Перейдите в директорию `fintech-short-assignments/03-etl/checker`.
```bash=
cd fintech-short-assignments/03-etl/checker
```

4. Запуск.
```bash=
sudo docker run -e PRIVATE_KEY=YOUR_KEY -e TRACEBACK=YOUR_TRACEBACK -v PATH_TO_SOLUTION:/data/input/student_response.txt -v "$PWD"/:/data/output/ ftchecker-etl:latest
```

## Прочее
- Около `0.5 POA` за проверку одного решения в тестовой сети `sokol`.
- Тест длится около `85 секунд`.
- Тестирет все обязательные методы и события описанные [на оффициальном сайте](https://ethereum.org/en/developers/docs/standards/tokens/erc-20/).
