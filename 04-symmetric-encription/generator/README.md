
# To generate checker

## 1. Edit `.env` file

|   |   |
|---|---|
| SECRETS  |  List of secrets for dumps, separated by 
semicolon ;. Amount of secrets is amount of dumps |
|  AMOUNT_OF_MESSAGES |  Number of messages for each client. (except handshake) |


## 2. Generate dumps

### 2.1 Clients sends random messages:
```bash
$ sudo TEST_SET=0 ./generate.sh
```
### 2.2 Clients use scenario
#### 2.2.1 set AMOUNT_OF_MESSAGES and AMOUNT_OF_CLIENT in .env file
#### 2.2.2 set SECRETS only one secret
#### 2.2.3 run `./generate.sh` with `--scenario` flag
```bash
$ sudo TEST_SET=0 ./generate.sh --scenario
```

Script will generate dumps in ./dumps$TEST_SET directory
for each secret in $SECRETS. 
+ File dump_$SECRET contains hex of dump of traffic.
+ File answer_$SECRET contains answer (sha256 of all messages of client)

3. Create checker for stepik for appropriate directory

```bash
$ python createChecker --dir ./dumps0
```

will save in ../checker/checker.py

