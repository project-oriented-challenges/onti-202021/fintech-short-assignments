import sys
from os import listdir
from os.path import isfile, join



def hex_it(filenames):
    for filename in filenames:
        with open(filename, 'rb') as f:
            content = f.read()
        with open(filename, 'w') as f:
            f.write(content.hex())
     
if __name__ == "__main__":
    if '--dir' in sys.argv:
        directory = sys.argv[sys.argv.index('--dir') + 1] 
        files = [join(directory, f) for f in listdir(directory) if isfile(join(directory, f))]
    else:
        files = sys.argv[1:]
    
    print(f'Hex {len(files)} files')
    hex_it(files)

    