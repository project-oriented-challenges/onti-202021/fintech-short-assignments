import os
from common.config import SERVER_PUBLIC_KEY, HELLO_MESSAGE, RANDOM_SEED

CONNECTION = os.environ.get('CONNECTION', '<None>')

HOST = os.environ.get('HOST', None)

PORT = int(os.environ.get('PORT', -1))

SECRET = str(os.environ.get('SECRET', None))

AMOUNT_OF_MESSAGES = int(os.environ.get('AMOUNT_OF_MESSAGES', 15))

DELAY = int(os.environ.get('DELAY', 1000))

CLIENT_LOG_TYPE = os.environ.get('CLIENT_LOG_TYPE', 'info')

N = int(os.getenv('N', -1))