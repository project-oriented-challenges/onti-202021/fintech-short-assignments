import logging
import random
import socket
import time
import sys
from typing import List, Union

from common.crypto import (decrypt, encrypt, get_random_key,
                           priv_to_pub, symmetric_key_decode,
                           symmetric_key_encode)

from config import (AMOUNT_OF_MESSAGES, CONNECTION, DELAY, HELLO_MESSAGE, HOST,
                    PORT, RANDOM_SEED, SECRET, SERVER_PUBLIC_KEY, CLIENT_LOG_TYPE, N)


class Client:
    def __init__(self, priv_key=None):
        self.host = None
        self.port = None
        self.connected = False
        self.priv_key: bytes = priv_key or get_random_key()

    @property
    def pub_key(self) -> bytes:
        return priv_to_pub(self.priv_key)

    def connect(self, host, port, connection_type):
        logging.info(
            f'Connect to server ({host},{port}, {connection_type}). ServerPubKey: {SERVER_PUBLIC_KEY}')
        self.host = host
        self.port = port
        assert connection_type in [
            'udp', 'tcp'
        ], f"Connection type should be udp or tcp. Not {connection_type}"
        self.connection_type = connection_type

        # Message with public key
        encrypted_my_pub_key = encrypt(
            HELLO_MESSAGE.encode() + self.pub_key,
            SERVER_PUBLIC_KEY,
        )
        encrypted_hello_respone = self.send_request(
            encrypted_my_pub_key,
        )
        decrypted_hello_respone = decrypt(
            encrypted_hello_respone,
            self.priv_key,
        )
        logging.info(f"hello response: {decrypted_hello_respone}")

        if decrypted_hello_respone.startswith(b"NOT OK"):
            raise ValueError(
                f"Response from server is not ok: {decrypted_hello_respone}")

        _, RND, KEY = decrypted_hello_respone.split()
        RND, KEY = int(RND), int(KEY)
        self.key = KEY
        rnd_response = self.send_request(
            symmetric_key_encode(str(RND + 1).encode(), self.key)
        )

        decoded_rnd_respone = symmetric_key_decode(rnd_response, self.key)
        self.connected = True
        return decoded_rnd_respone

    def send_to_server(self, message: bytes):
        assert self.connected, 'You\'re not connected. execute connect() first'
        encoded_message = symmetric_key_encode(message, self.key)
        encoded_response = self.send_request(encoded_message)
        logging.debug(f"raw response: {encoded_response!r}")
        return symmetric_key_decode(encoded_response, self.key)

    def send_request(self, message: Union[str, bytes]) -> bytes:
        if isinstance(message, str):
            message = bytes(message, 'ascii')
        elif isinstance(message, bytes):
            pass
        else:
            raise TypeError(
                f"message should either str or bytes (not {type(message)})")

        if self.connection_type == 'tcp':
            return self.send_request_tcp(message)

        elif self.connection_type == 'udp':
            return self.send_request_udp(message)
        else:
            raise ConnectionError('No connection. execute connect() first')

    def send_request_tcp(self, message: bytes) -> bytes:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((self.host, self.port))
            sock.sendall(message)
            response = sock.recv(1024)
            return response

    def send_request_udp(self, message: bytes) -> bytes:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.sendto(message, (self.host, self.port))
            received = sock.recv(1024)
            return received


def insert_into_string(source_str, insert_str, pos):
    return source_str[:pos] + insert_str + source_str[pos + len(insert_str):]


if __name__ == "__main__":
    logging.basicConfig(level=(logging.DEBUG if CLIENT_LOG_TYPE == 'debug' else logging.INFO))
    if '--scenario' in sys.argv:
        messages: List[str] = open(f'./scenario/{N}').readlines()            
    else:
        messages: List[str] = open('messages.txt').readlines()
        random.shuffle(messages)
        messages = messages[:AMOUNT_OF_MESSAGES]
        messages[0] = insert_into_string(messages[0], SECRET, len(messages[0])//3)
        logging.debug(f"Client's secret message: {messages[0]}")
        random.shuffle(messages)
    
    messages = list(map(bytes.strip, map(str.encode, messages)))

    logging.debug(f"all messages: {messages}")
    client = Client()

    time.sleep(1)
    logging.info(
        f"Start client with private key {client.priv_key!r} and pub_key {client.pub_key!r}")
    response = client.connect(HOST, PORT, CONNECTION)
    logging.debug(f"Connected: {client.connected}. response: {response}")
    time.sleep(1)

    for i, message in enumerate(messages):
        logging.debug(f"Send message #{i} ({message}) to server")
        response = client.send_to_server(message)
        logging.debug(f"Response: {response}")

        time.sleep(random.randint(DELAY // 2, DELAY * 2) / 1000)

    time.sleep(1)
