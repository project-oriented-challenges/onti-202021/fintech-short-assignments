#/bin/bash

# Read variables from .env file
source `pwd`/.env

amount_of_packages=$((($AMOUNT_OF_MESSAGES + 2) * 2 * $AMOUNT_OF_CLIENTS))

echo "Generate dump for every word in \$SECRETS: $SECRETS"

IFS=';'
read -ra secrets_array <<< "$SECRETS"
IFS=' '


docker-compose down
args=$1 docker-compose up -d server

for i in "${secrets_array[@]}"
do
    
    answer_client=$((1 + RANDOM % $AMOUNT_OF_CLIENTS))
    mkdir "./dumps$TEST_SET" > /dev/null
    
    echo "----------------------------------------------------"
    echo "Create dump_$i. client with phrase is $answer_client"
    echo "----------------------------------------------------"
    
    touch "./dumps$TEST_SET/dump_$i"
    sudo chmod 666 "./dumps$TEST_SET/dump_$i"
    sudo tcpdump -i "ft_network" -w "./dumps$TEST_SET/dump_$i" -c $amount_of_packages "udp and port 5432" & 

    # Set secret variable. SECRET_j for j-th client
    for (( j = 1; j <= $AMOUNT_OF_CLIENTS; j+=1 )); do
        if (( j==$answer_client ))
        then
            export SECRET_$j="$i"
        else
            export SECRET_$j=""
        fi
    done
    
    echo "Start clients"

    args=$1 docker-compose up client1 client2 client3 client4 client5 client6    
    
    python hex_it.py "./dumps$TEST_SET/dump_$i"
    docker exec generator_server_1 python get_messages_hash.py $answer_client > "./dumps$TEST_SET/answer_$i"
    echo "Done."
done
echo "Save logs from server in ./dumps$TEST_SET/server.log" 
docker logs generator_server_1 &> ./dumps$TEST_SET/server.log
