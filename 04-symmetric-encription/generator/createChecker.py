from os import listdir
from os.path import isfile, join
import sys
import hashlib
from typing import List, Dict, Tuple, Iterable

def sha256(data: Iterable[str]) -> str:
    return hashlib.sha256(b''.join(map(str.encode, data))).hexdigest()

try:
    dumps_directory = sys.argv[sys.argv.index('--dir') + 1]
    assert not dumps_directory.startswith('--') 
except (ValueError, IndexError) as e:
    raise ValueError('specify directory using --dir flag')

try:
    checker_path = sys.argv[sys.argv.index('--checker') + 1]    
    assert not checker_path.startswith('--') 
except (ValueError, IndexError) as e:
    raise ValueError('specify directory using --checker flag')



all_files: List[str] = [f for f in listdir(dumps_directory) if isfile(join(dumps_directory, f))]

dumps: Dict[str, List[str]] = {}

for filename in all_files:
    path_to_file = join(dumps_directory, filename)
    _, _, secret = filename.partition('_')
    
    if filename.startswith('dump'):
        dumps.setdefault(secret, ['', ''])
        dumps[secret][0] = open(path_to_file, 'r').read()

    elif filename.startswith('answer'):
        dumps.setdefault(secret, ['', ''])
        dumps[secret][1] = open(path_to_file, 'r').read().strip()

with open(checker_path, 'w') as file:
    file.write('''
from random import choice
import hashlib
from typing import Iterable

def sha256(data: Iterable[str]) -> str:
    return hashlib.sha256(b''.join(map(str.encode, data))).hexdigest()
dumps = [
''')

    for secret in dumps:
        file.write(f"'{secret}\\n{dumps[secret][0]}'" + ',\n')
    
    file.write('''
]
answers = {
''')

    for secret in dumps:
        question = secret + '\n' + dumps[secret][0]
        file.write(f"'{sha256([question])}':'{dumps[secret][1]}'" + ',\n')

    file.write('''}

def generate():
    return choice(dumps)


def solve(dataset): 
    return answers[sha256(dataset)]


def check(reply, clue):
    return reply.strip() == clue.strip()

a = dumps.pop(0)

tests = [
    (a, solve(a), solve(a))
]
''')