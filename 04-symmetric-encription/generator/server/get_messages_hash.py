import sys
from hashlib import sha256

if __name__ == "__main__":
    client_number = int(sys.argv[1])
    with open('client' + str(client_number), 'rb') as f:
        data = f.read()[:-1]
    print(sha256(data).hexdigest())