import logging
import socketserver

import config
import sys
from common.crypto import decrypt, encrypt, symmetric_key_decode, symmetric_key_encode
from typing import Dict
import random
from os import listdir
from os.path import isfile, join

random.seed(config.RANDOM_SEED)

database: Dict[str, int] = {}

def client_number(ip: str) -> int:
    return int(ip.split('.')[-1]) % 50

def log_message(data, ip):
        number = client_number(ip)
        with open('client' + str(number), 'ab') as f:
            f.write(data + b'\n')

def process_data(data: bytes, ip: int) -> bytes:
    def process_symmetric_message(message: bytes, ip: int) -> bytes:        
        n = client_number(ip)
        
        try:
            return_message = scenario[n].pop(0).encode()
            if return_message == b'%DO NOTHING%':
                return_message = None
            
        except (IndexError, TypeError):
            return_message = message[::-1]

        if return_message:
            log_message(return_message, ip)

        return return_message
    # encrypted with my pubkey
    try:
        message_from_client: str = decrypt(data, config.SERVER_PRIVATE_KEY).decode()
        logging.info(f'Hello message: {message_from_client}')
        _, _, client_pub_key = message_from_client.partition('\n')
    except:
        client_pub_key = ''
    
    if client_pub_key:
        logging.debug(f"client_pub_key {client_pub_key}")
        rnd, key = random.randint(2**63, 2**64), random.randint(config.start, config.end)
        database[ip] = key

        with open('client' + str(client_number(ip)), 'wb') as f:
            f.write(b'')
        
        return_data = ('\n'.join(['OK', str(rnd), str(key)])).encode()
        return encrypt(return_data, client_pub_key)
    else:
        # encrypted with symmetric cipher
        try:
            key = database[ip]
        except KeyError:
            return b"NOT OK. AND DID NOT CONNECT"
        
        logging.debug(f"decode message for ip: {ip}, and key: {key}")
        message = symmetric_key_decode(data, key)
        logging.debug(f"message decoded: {message}")
        # Just reverse message
        return symmetric_key_encode(process_symmetric_message(message, ip), key)


class MyUDPHandler(socketserver.BaseRequestHandler):
    """
    This class works similar to the TCP handler class, except that
    self.request consists of a pair of data and client socket, and since
    there is no connection the client address must be given explicitly
    when sending data back via sendto().
    """

    def handle(self):
        try:
            self.data = self.request[0]
            socket = self.request[1]
            logging.info(f"{self.client_address[0]} wrote: {self.data}")
            ip = self.client_address[0]
            processed_data = process_data(self.data, ip)
            logging.debug(f"Send to client data: {processed_data}")
            socket.sendto(processed_data, self.client_address)
        except Exception as e:
            logging.error(e)
            socket.sendto(b"NOT OK. error: " + e.__str__().encode(), self.client_address)

    

class MyTCPHandler(socketserver.StreamRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        try:
            self.data = self.request.recv(1024).strip()
            logging.debug(f"{self.client_address[0]} wrote {self.data}")
            ip = self.client_address[0]
            processed_data = process_data(self.data, ip)
            self.request.sendall(processed_data)
        except Exception as e:
            logging.error(e.__str__())


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    HOST, PORT = "0.0.0.0", config.PORT

    if '--scenario' in sys.argv:
        directory = './scenario'
        files = [f for f in listdir(directory) if isfile(join(directory, f))]
        scenario = {int(f):list(map(str.strip, open(join(directory, f)).readlines())) for f in files}
        logging.debug(f"Scenario: {scenario}")
    else:
        scenario = None

    if config.CONNECTION == 'udp':
        with socketserver.UDPServer((HOST, PORT), MyUDPHandler) as server:
            server.serve_forever()
    
    elif config.CONNECTION == 'tcp':
        with socketserver.TCPServer((HOST, PORT), MyTCPHandler) as server:
            server.serve_forever()

    else:
        raise Exception(f"Connection should be tcp or udp. Not {config.CONNECTION}")
