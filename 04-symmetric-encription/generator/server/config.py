import os
from common.config import SERVER_PRIVATE_KEY, HELLO_MESSAGE, RANDOM_SEED

CONNECTION = os.environ.get('CONNECTION', None )

HOST = os.environ.get('HOST', None)
PORT = int(os.environ.get('PORT', -1))

start, end = 2**32 - 2**22, 2**32
