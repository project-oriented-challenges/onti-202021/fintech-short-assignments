import seccure
from base64 import b64encode, b64decode
import string
from base64 import b64decode as b_decode, b64encode as b_encode
import random


def encrypt(message: bytes, pub_key: bytes) -> bytes:
   return seccure.encrypt(message, pub_key)

def decrypt(encoded_message: bytes, priv_key) -> bytes:
   return seccure.decrypt(encoded_message, priv_key)

def get_random_key(keysize=64) -> bytes:
   letters = string.ascii_letters
   random_letters = ''.join(random.choice(letters) for i in range(keysize))
   return random_letters.encode()

def priv_to_pub(priv_key: bytes) -> bytes:
   return str(seccure.passphrase_to_pubkey(priv_key)).encode()


def wrap(s, w):
    '''
    'abcdef', 3 -> ['abc', 'def']
    '''
    return [s[i:i + w] for i in range(0, len(s), w)]

def symmetric_key_decode(string: bytes, key: int, m=2038074743) -> bytes:
   decoded_string = bytes()
   for chunk in wrap(string, 4):
      decoded_string += ((int(chunk.hex(), 16) - key) % m).to_bytes(4, 'big')
   return b_decode(decoded_string)

def symmetric_key_encode(string: bytes, key: int, m=2038074743, debug=False) -> bytes:
   b64string = b_encode(string)
   encoded_string = bytes()
   for chunk in wrap(b64string, 4):
      new_chunk = ((int(chunk.hex(), 16) + key) % m).to_bytes(4, 'big')
      if debug:
         print(f"{chunk} -> {new_chunk.hex()}", end=', ')
      encoded_string += new_chunk
   if debug:
      print()
   return encoded_string
