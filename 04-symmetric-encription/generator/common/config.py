import os
import logging
import seccure
import random

HELLO_MESSAGE = 'Hello from client! My public key:\n'

SERVER_PRIVATE_KEY = str(os.environ.get('SERVER_PRIVATE_KEY', '')).encode()

SERVER_PUBLIC_KEY = str(seccure.passphrase_to_pubkey(SERVER_PRIVATE_KEY)).encode()

RANDOM_SEED = int(os.environ.get('RANDOM_SEED', '') or random.randint(0, 10000000))