import struct
from base64 import b64decode, b64encode
from hashlib import sha256
lines = []

with open('input.txt', 'r') as f:
    searched1 = f.readline()[:-1]
    pcapCaptured = f.readline()[:-1]


info_log = []
senders = []
getters = []
addresses_dumped = []
i = 0


# Decrypror
def decrypter(data: bytes, key: int, m=2038074743):
    decoded_string = bytes()
    for i in range(0, len(data), 4):
        decoded_string += ((int(data[i:i+4].hex(), 16) - key) % m).to_bytes(4, 'big')
    return b64decode(decoded_string)


def patternGenerator(data:str, key: int, m=2038074743):
    encoded_string_1 = bytes()
    encoded_string_2 = bytes()
    encoded_string_3 = bytes()
    count_init = len(data)
    b_data1 = bytes(data, 'ascii')
    b_data2 = bytes('_' + data, 'ascii')
    b_data3 = bytes('__' + data, 'ascii')

    data_1 = b64encode(b_data1)
    data_2 = b64encode(b_data2)
    data_3 = b64encode(b_data3)

    for i in range(0, len(data_1), 4):
        encoded_string_1 += ((int(data_1[i:i+4].hex(), 16) + key) % m).to_bytes(4, 'big')
    for i in range(0, len(data_2), 4):
        encoded_string_2 += ((int(data_2[i:i+4].hex(), 16) + key) % m).to_bytes(4, 'big')
    for i in range(0, len(data_3), 4):
        encoded_string_3 += ((int(data_3[i:i+4].hex(), 16) + key) % m).to_bytes(4, 'big')
    return [encoded_string_1.hex(), encoded_string_2.hex()[4:-3 + (-1*count_init // 4)], encoded_string_3.hex()[6:-3 + (-1*(count_init//4))]]


# PCAP-file parser
text2 = pcapCaptured[48:]
while text2 != '':
    i += 1
    header_len_start = bytes.fromhex(text2[16:24])
    z = struct.unpack('=i', header_len_start)
    jump = (z[0])*2 + 32

    data_start = 84 + 32
    data_end = jump

    getter = text2[32:44]
    sender = text2[44:56]
    data = text2[data_start:data_end]

    text2 = text2[jump:]
    info_log.append([sender, getter, data])
    senders.append(sender)
    getters.append(getter)
# END PARSER


# Traffic analyser
for i in range(len(info_log)):
    if [senders[i], getters[i]] not in addresses_dumped:
        addresses_dumped.append([senders[i], getters[i]])

message_texts = ['']*len(addresses_dumped)

for i in range(len(info_log)):
    index = addresses_dumped.index([info_log[i][0], info_log[i][1]])
    message_texts[index] += info_log[i][2]

# Jump key generator
patterns1 = []
answer = ''
key = 0
key2 = 0
flag = False
mesages = []
for i in range(2**32 - 2**22, 2**32):
    genned_pattern = patternGenerator(data=searched1, key=i)
    for j in range(len(message_texts)):
        if genned_pattern[0] in message_texts[j] or genned_pattern[1] in message_texts[j] or genned_pattern[2] in message_texts[j]:
            key = i
            cutter = 0
            for k in range(len(info_log)):
                if info_log[k][0] == addresses_dumped[j][1] and info_log[k][1] == addresses_dumped[j][0]:
                    if cutter == 0:
                        cutter = 1
                    else:
                        mesages.append(info_log[k][2])
            flag = True
            break
    if flag:
        break

# Message decryption
print(key)
answer = str()
print(mesages)
for message in mesages:
    try:
        answer += decrypter(bytes.fromhex(message), key=key).decode('ascii')
        answer += '\n'
    except Exception:
        print('ERROR', message)
answer = answer[:-1]
print(answer)
print(bytes(answer, 'ascii'))
# SHA-256 encryptyon
answer = sha256(bytes(answer, 'ascii')).hexdigest()
print(answer)

