Sample test description
===

|         | Symmteric key | ip           | public key                     | private key                                                           |
| ------- | ------------- | ------------ | ------------------------------ | --------------------------------------------------------------------- |
| Client1 | 4294712856    | 144.255.0.51 | `b'D-=u*s|SkRh5_CNzDf0DPKIRz'` | `b'gAAPlTJmHwzossbQQFkohZQvCTcUKnunStNbBTzBrGZzzhMrWYmYzzHjklfjsEpw'` |
| Client2 | 4292821837    | 144.255.0.52 | `b')3=}Fu3Ete#X1EBW8xh>!8M[B'` | `b'dCTHGTjcFiDEMUyfCxlmTpkmSfeYdcBYsPlHrspxNeQyzfyjjRtXYeGAoRUUiqYM'` |
| Client3 | 4293660400    | 144.255.0.53 | `b'-0mHICp4qJ@[k{BM?#G]Sn5#E'` | `b'AJKWbHuoUtRSyPqGcjMobFwkdhyohWdDgFIvThogYzNPwFtYnTSMCocsYavyGHav'` |
| Server  | -             | 144.255.0.77 | `b'5^-?-*7;x%QT9AZgrRnJ(fad5'` | `b'supersecretpassphrase'`                                            |

Encoded message from client1:
`b'cNk\x92qJE\x9eq_Q\x9af\x80E\x99f_U\x99VNH\x91gtI\xa3VN4\xa0ptX\x91pN{\x96f_Q\x9aotf\x91p9<[otTa'`
