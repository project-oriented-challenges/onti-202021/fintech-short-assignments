from random import choice

data = {"dashvayet/fintech2020node1": "198",
        "dashvayet/fintech2020node2": "208",
        "dashvayet/fintech2020node3": "148",
        "dashvayet/fintech2020node4": "115",
        "dashvayet/fintech2020node5": "105",
        "dashvayet/fintech2020node6": "80",
        "dashvayet/fintech2020node7": "80",
        "dashvayet/fintech2020node8": "75",
        "dashvayet/fintech2020node9": "106",
        "dashvayet/fintech2020node10": "76",
        "dashvayet/fintech2020node11": "156"
}

def generate():
    return choice(list(data.keys())[:-1]) + "\n"

def solve(dataset):
    return data[dataset.strip()]

def check(reply, clue):
    return int(reply) == int(clue)

tests = [
    ("dashvayet/fintech2020node11\n", "156", "156")
]