import random
import json
from web3 import Web3
from eth_account import Account
web3 = Web3(Web3.HTTPProvider('http://0.0.0.0:8545'))
blockNum = web3.eth.blockNumber
senders = []
all = []
for i in range(blockNum+1):
    block = web3.eth.getBlock(i)
    try:
        transaction = web3.eth.getTransaction(block["transactions"][0].hex())
        sender = transaction["from"]
        receiver = transaction["to"]
        if sender not in senders:
            senders.append(receiver)
        if receiver not in all:
            all.append(receiver)
        if sender not in all:
            all.append(sender)
    except Exception as e:
        print(e)
answer = 0
print(all)
print(len(all))
for i in range(len(all)):
    if web3.eth.getBalance(all[i]) != 0:
        answer += 1
print(answer)
# solution code here
