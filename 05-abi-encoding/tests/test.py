from decouple import config

from checker import checker
from solution.source import solve as my_solve

DEBUG = config("DEBUG", bool)

# Test stepic tests
for test in checker.tests:
    assert checker.solve(test[0]) == test[1]
    assert checker.solve(test[0]) == test[2]

# Test ideal solution
for data, _ in checker.data.items():
    res_my_solve = my_solve(data)
    res_solve = checker.solve(data)
    if DEBUG:
        print("    MY_SOLVE:     " + res_my_solve)
        print("    RIGHT ANSWER: " + res_solve)
    assert checker.check(res_solve, res_my_solve)
