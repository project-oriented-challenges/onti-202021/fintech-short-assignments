import json
from collections.abc import Iterable
from random import randint, choice, sample, shuffle

from decouple import config
from web3 import Web3
from web3.contract import Contract, HexBytes
from web3.datastructures import AttributeDict

eventNames = ['Approve',
              'Transfer',
              'Approved',
              'Transferred',
              'Executed',
              'Logged',
              'Incremented',
              'Registered',
              'Unregistered',
              'Claimed',
              'Withdrawal',
              'Deposit',
              'Canceled',
              'Removed',
              'Finalized',
              'Created',
              'Executed',
              'Confirmed',
              'Slashed',
              'Voted',
              'Purchased',
              'Ordered',
              'Sent'
              ]

typesNames = ['bytes32', 'uint256', 'bool', 'address']

DEBUG = config("DEBUG", default=False, cast=bool)
CONTRACT_ADDRESS = "0x6156bED5e3Ca36fB40DD22206dF70837aaE8D57f"
PRIVATE_KEY = config("PRIVATE_KEY")
DATASET_LEN = config("DATASET_LEN", default=25, cast=int)
NODE_URL = "https://sokol.poa.network"

with open("test_bytecode") as f:
    bytecode = f.readline()

with open("test_abi") as f:
    abi = json.load(f)

w3 = Web3(Web3.HTTPProvider(NODE_URL))
my_account = w3.eth.account.from_key(PRIVATE_KEY)
w3.eth.defaultAccount = my_account.address
w3.eth.account = my_account
myContract = w3.eth.contract(address=CONTRACT_ADDRESS, abi=abi, bytecode=bytecode)


def get_nonce():
    return w3.eth.getTransactionCount(w3.eth.defaultAccount)


def call_contract_function(contract: Contract, func_name: str, nonce: None, *args, **kwargs):
    """
   Calls method in contract
   :return hash of transaction
   """
    if nonce is None:
        nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)
    method = getattr(contract.functions, func_name)
    trans = method(*args, **kwargs).buildTransaction({"nonce": nonce})
    signed_trans = w3.eth.account.sign_transaction(trans)
    trans_hash = w3.eth.sendRawTransaction(signed_trans.rawTransaction)
    return trans_hash


def wait_to_be_mined(hashes):
    """
    Function waits while all transactions will be mined
    :param hashes: hashes of transaction(s) to be mined
    :return: receipts of transactions
    """
    if isinstance(hashes, HexBytes):
        hashes = [hashes]
    receipts = []
    while len(receipts) != len(hashes):
        for trans_hash in hashes:
            if not any([r.transactionHash == trans_hash for r in receipts]):
                try:
                    receipt = w3.eth.getTransactionReceipt(trans_hash)
                    receipts.append(receipt)
                    if DEBUG:
                        print("Transaction mined: " + trans_hash.hex())
                except:
                    pass
    return receipts


def replace_web3_structures(iterable):
    """
    Utility method for replacing all unserializable structures in
    object to python one's. Used for correct serialization to JSON.
    :param iterable: iterable or key-accessible structure
    :return: iterable structure with replaced data structures
    """
    if not isinstance(iterable, Iterable):
        return iterable
    if isinstance(iterable, AttributeDict):
        iterable = dict(iterable)
    if isinstance(iterable, tuple):
        iterable = list(iterable)
    if isinstance(iterable, HexBytes):
        return iterable.hex()
    if isinstance(iterable, str):
        return iterable
    if hasattr(iterable, "keys"):
        for key in iterable.keys():
            elem = iterable[key]
            if isinstance(elem, Iterable):
                res = replace_web3_structures(elem)
                iterable[key] = res
    else:
        for i in range(len(iterable)):
            elem = iterable[i]
            if isinstance(elem, Iterable):
                res = replace_web3_structures(elem)
                iterable[i] = res

    return iterable


# def contract_register_event(desc: HexBytes, data: bytes):
#     myContract.functions.registerEvent().buildTransaction({"nonce": nonce})
#     signed_trans = w3.eth.account.sign_transaction(trans)
#     trans_hash = w3.eth.sendRawTransaction(signed_trans.rawTransaction)


def generateEvent():
    trimmed_params = ''
    params = ''
    event_data = b''

    num_parameters = randint(2, 6)

    if num_parameters != 0:
        for i in range(num_parameters):
            t = choice(typesNames)

            trimmed_params = trimmed_params + f'{t},'
            params = params + f'{t}, '

            if t == 'bytes32':
                data = randint((2 ** 240) + 1, (2 ** 256) - 1)
            elif t == 'address':
                data = randint((2 ** 144) + 1, (2 ** 160) - 1)
            elif t == 'unit256':
                data = randint(0, (2 ** 256) - 1)
            else:
                data = randint(0, 1)
            event_data = event_data + data.to_bytes(32, 'big')

        params = params[:-2]
        trimmed_params = trimmed_params[:-1]

    e = choice(eventNames)
    eventDeclaration = f'event {e}({params});'
    eventDecr = Web3.keccak(text=f'{e}({trimmed_params})')

    return e, eventDeclaration, eventDecr, event_data


dataset = dict()
print("Starting generation of dataset")
for i in range(DATASET_LEN):
    events_in_tx = {}
    events_per_tx = randint(1, 5)
    count = 0
    hashes = []
    nonce = get_nonce()
    while count < events_per_tx:
        (name, decl, desc, data) = generateEvent()
        if name not in events_in_tx:
            events_in_tx[name] = decl
            # call registerEvent from contract with desc and data
            trans_hash = call_contract_function(myContract, "registerEvent", nonce + count, desc, data)
            hashes.append(trans_hash)
            count = count + 1
    receipts = wait_to_be_mined(hashes)
    # call emitEvents and collect transaction receipt
    emit_events_hash = call_contract_function(myContract, 'emitEvents', None)
    emit_events_receipt = wait_to_be_mined(emit_events_hash)

    challanged_event = choice(list(events_in_tx.keys()))

    possible_events = set(sample(eventNames - events_in_tx.keys(), 9))
    events_list = list({challanged_event}.union(possible_events))
    shuffle(events_list)

    # concatenate events_list and transaction receipt as the test data, events_in_tx[challanged_event] is the clue
    test_data = ' '.join(events_list) + '\n' + json.dumps(replace_web3_structures(emit_events_receipt[0]))
    clue = events_in_tx[challanged_event]
    dataset[test_data] = clue
    print(f"{i + 1} of {DATASET_LEN} datum generated")

print(dataset)
with open("data", 'w+') as f:
    if DEBUG:
        [(print(key), print(value)) for key, value in dataset.items()]
    print()
    print("Done. Check file `data` to get results")
    f.write(str(dataset))
