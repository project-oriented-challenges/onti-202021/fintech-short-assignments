import json
from itertools import product

from web3 import Web3


def solve(data):
    keccak = Web3.keccak
    DATA_TYPES = ['uint256', 'address', 'bytes32', 'bool']

    words, receipt = data.split('\n', maxsplit=1)
    receipt = json.loads(receipt)
    logs = receipt['logs']
    words = words.split()

    def bruteforce():
        for num_of_args in range(100):
            for log in logs:
                topics = log['topics']
                for word in words:
                    combinations = (p for p in product(DATA_TYPES, repeat=num_of_args))
                    for combination in combinations:
                        str_args = ','.join(combination)
                        str_to_test = f'{word}({str_args})'
                        if keccak(text=str_to_test).hex() == topics[0]:
                            return str_to_test, word, combination, topics
        return '', '', (), ()

    stripped_answer, event_name, args, topics = bruteforce()
    if stripped_answer == '':
        # If bruteforce doesn't found correct answer, raise exception
        raise Exception
    args_str = ", ".join(args)
    final_string = f"event {event_name}({args_str});"
    return final_string
