# Hierarchical Deterministic Wallet
Генератор тестовых данных
====
Чтобы сгенерировать тестовые данные для решения задачи, необходимо вызвать из файла `generator/checker.py` функцию `generate()`. Чтобы получить правильный ответ, необходимо из этого же файла вызвать функцию `solve(dataset)`, на вход необходимо подать тестовые данные.
Пример тестовых данных:
```
590
sing vote collect system buddy obtain correct burst tuition effort ankle scare 421
hat coral arrange around leaf rack hat sadness young modify industry forward 345
giant easily face crew depth art token remind program always cheap business 938
spawn whip track flavor window eagle crawl offer dismiss clinic service card 365
hotel foil blanket joy ticket tiny curious winter license absent business oxygen 226
mouse battle obvious garment shell moon basic energy aunt rely giraffe huge 1006
sick door cousin romance pottery electric rebel call river fat river oak 197
arrange idle ripple retreat slight glide inmate bullet range wet talent flight 866
october grunt oil this unfair aisle tumble fame pig airport stove little 1008
whisper normal voice expose humble autumn silk radio infant custom charge eager 994
gift cool siren curious average genre music gold spoil sister license faint 854
cigar street disorder push foot unaware save alter depend feed more dentist 89
era figure odor pass fold toast walnut rural blush make industry dumb 58
manage kite atom depend thing episode injury aspect airport day crash settle 576
sad appear drama host candy craft useful menu offer fat hurt wild 561
scatter ancient injury virtual tower venture skull doctor lunch list force fringe 187
```
Solution
====
В коде `solution/source.py` есть функция `solve(filename)`, где *`filename - путь к файлу с тестовыми данными`*, которая отвечает за решение задачи путем непосредственного обращения к ноде и запрашивания баланса для каждого из аккаунтов. 
