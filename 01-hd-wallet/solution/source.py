from web3 import Web3, HTTPProvider

node = Web3(HTTPProvider("http://165.227.146.243:3000"))

node.eth.account.enable_unaudited_hdwallet_features()

def get_balance(address, block_number=None):
    return node.eth.getBalance(address, block_number)

def solve(filename):
    file = open(filename, 'r')
    lines = file.read().split('\n')
    block = int(lines[0])
    balances = []
    for ln in lines[1:-1]:
        lns = ln.split()
        path = "m/44'/60'/0'/0/"+lns[-1]
        words = ' '.join(lns[:-1])
        acc = node.eth.account.from_mnemonic(words,account_path=path)
        balances.append(get_balance(acc.address, block_number=block))
    
    file.close()
    return max(balances)
    
