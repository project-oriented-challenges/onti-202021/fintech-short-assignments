from web3 import Web3, HTTPProvider
from random import randint
import sys
def send_money(address, amount):
    nonce = node.eth.getTransactionCount(ADMIN_ADDR)

    tx = {'to': address,
              'gas': 1000000,
              'gasPrice': Web3.toWei(100, 'gwei'),
              'chainId': 77,
              'value': amount,
              'nonce': nonce
              }
    signed = ADMIN_ACC.signTransaction(tx)
    tx = node.eth.sendRawTransaction(signed.rawTransaction)
    rec = node.eth.waitForTransactionReceipt(tx)
    return rec
    
def get_balance(address, block_number=None):
    return node.eth.getBalance(address, block_number)
    

node = Web3(HTTPProvider("http://165.227.146.243:3000"))
ADMIN_ADDR = '0x0' #вместо 0х0 вставить адрес аккаунта с которого будут проводиться транзакции
ADMIN_ACC = node.eth.account.from_key('0x0') #вместо 0х0 вставить приватный ключ аккаунта с которого будут проводиться транзакции

node.eth.account.enable_unaudited_hdwallet_features()

for num in range(20):
    acc_to_words = {}
    filename = 'second_' + str(num).zfill(2) + '.txt'
    filename_a = 'second_' + str(num).zfill(2) + '_a.txt'
    
    for i in range(randint(10, 20)):
        path = "m/44'/60'/0'/0/"+str(randint(0, 1024))
        acc, words = node.eth.account.create_with_mnemonic(num_words=12, account_path=path)
        acc_to_words[acc.address] = [words, path]
    txs = []
    nonce = node.eth.getTransactionCount(ADMIN_ADDR)
    for address in acc_to_words:
        amount = randint(524, 1025800)
        tx = {'to': address,
                  'gas': 21000,
                  'gasPrice': Web3.toWei(0.01, 'gwei'),
                  'chainId': 2021,
                  'value': amount, 
                  'nonce': nonce
                  }
        signed = ADMIN_ACC.signTransaction(tx)
        txs.append(signed)
        nonce += 1
    sent = []
    for signed in txs:
        sent.append(node.eth.sendRawTransaction(signed.rawTransaction))
    rec = []
    for tx in sent:
        _rec = node.eth.waitForTransactionReceipt(tx)
        rec.append(_rec)
    
    block_number = rec[-1]['blockNumber']
    file = open(filename, 'w')
    file.write(str(block_number) + '\n')
    for i in acc_to_words:
        file.write(acc_to_words[i][0])
        file.write(' ' + acc_to_words[i][1].split('/')[-1] + '\n')
    file.close()
    balances = []
    for i in acc_to_words:
        balances.append(get_balance(i, block_number=block_number))
    
    file = open(filename_a, 'w')
    file.write(str(max(balances)))
    file.close()
