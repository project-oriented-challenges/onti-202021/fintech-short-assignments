from collections import defaultdict

from eth import constants
from eth.chains.base import MiningChain
from eth.db.atomic import AtomicDB
from eth.vm.forks.istanbul import IstanbulVM, IstanbulState
from eth_keys import keys
from eth_utils import encode_hex, decode_hex
from web3 import Web3, HTTPProvider

w3 = Web3(HTTPProvider('http://sokol.poa.network'))

database = defaultdict(dict)
addresses = set()


class OnlineState(IstanbulState):
    def get_code(self, address):
        print(f'Getting code for {encode_hex(address)}')
        addresses.add(address)
        return w3.eth.getCode(address)

    def get_storage(self, address, slot, from_journal=True):
        if slot not in database[address]:
            value = w3.eth.getStorageAt(address, slot)
            value = int.from_bytes(value, 'big')
            self.set_storage(address, slot, value, True)
        else:
            value = database[address][slot]
        print(f'Loading storage for {encode_hex(address)} at {slot} with value {encode_hex(value.to_bytes(32, "big"))}')
        return value

    def set_storage(self, address, slot, value, from_get=False):
        if not from_get:
            print(f'Saving storage for {encode_hex(address)} at {slot}')
        database[address][slot] = value


class OnlineVM(IstanbulVM):
    _state_class = OnlineState


class OnlineChain(MiningChain):
    chain_id = 123
    vm_configuration = ((0, OnlineVM),)


chain = OnlineChain.from_genesis(
    AtomicDB(),
    {
        'parent_hash': constants.GENESIS_PARENT_HASH,
        'uncles_hash': constants.EMPTY_UNCLE_HASH,
        'coinbase': constants.ZERO_ADDRESS,
        'transaction_root': constants.BLANK_ROOT_HASH,
        'receipt_root': constants.BLANK_ROOT_HASH,
        'difficulty': constants.GENESIS_DIFFICULTY,
        'block_number': 18649394,
        'gas_limit': 1000000000,
        'extra_data': constants.GENESIS_EXTRA_DATA,
        'nonce': constants.GENESIS_NONCE
    }
)
vm = chain.get_vm()
state = vm.state


def solve(addr, data):
    global addresses
    contract = decode_hex(addr)
    data = decode_hex(data)

    private_key = keys.PrivateKey(w3.eth.account.create().privateKey)

    tx = vm.create_unsigned_transaction(
        nonce=0,
        gas_price=0,
        gas=900000000,
        to=contract,
        value=0,
        data=data
    )
    signed_tx = tx.as_signed_transaction(private_key)
    chain.apply_transaction(signed_tx)

    addresses = sorted(addresses)
    addresses = [w3.toChecksumAddress(encode_hex(address)) for address in addresses]
    print('Answer: ')
    print(' '.join(addresses))


solve('0xE7e55bbb6f40444843a195f0E42797a80b7687E7',
      '0x<eth_call method call with parameters in hexadecimal form>')
