import random

def generate():
    keys = list(data.keys())[1:]
    return random.choice(keys)


def solve(dataset):
    return ' '.join(data[str(dataset)])
    

def check(reply, clue):
    return set(reply.split()) == set(clue.split())


line = list(data.keys())[0]
exposed = line
tests = [
    (exposed, solve(exposed), solve(exposed))
]
