from web3 import Web3, HTTPProvider
import random
from collections import defaultdict

from eth import constants
from eth.chains.base import MiningChain
from eth.db.atomic import AtomicDB
from eth.vm.forks.istanbul import IstanbulVM, IstanbulState
from eth_keys import keys
from eth_utils import encode_hex, decode_hex

sokol = Web3(HTTPProvider("https://kovan.infura.io/v3/api_key_put_here_"))
ADMIN_ACC = sokol.eth.account.from_key('')  # put private key here

ch_id = sokol.eth.chain_id()

w3 = sokol

database = defaultdict(dict)
addresses = set()


class OnlineState(IstanbulState):
    def get_code(self, address):
        print(f'Getting code for {encode_hex(address)}')
        addresses.add(address)
        return w3.eth.getCode(address)

    def get_storage(self, address, slot, from_journal=True):
        if slot not in database[address]:
            value = w3.eth.getStorageAt(address, slot)
            value = int.from_bytes(value, 'big')
            self.set_storage(address, slot, value, True)
        else:
            value = database[address][slot]
        print(f'Loading storage for {encode_hex(address)} at {slot} with value {encode_hex(value.to_bytes(32, "big"))}')
        return value

    def set_storage(self, address, slot, value, from_get=False):
        if not from_get:
            print(f'Saving storage for {encode_hex(address)} at {slot}')
        database[address][slot] = value


class OnlineVM(IstanbulVM):
    _state_class = OnlineState


class OnlineChain(MiningChain):
    chain_id = 123
    vm_configuration = ((0, OnlineVM),)


def send_money(address, amount):
    node = sokol
    nonce = node.eth.getTransactionCount(ADMIN_ACC.address)

    tx = {'to': address,
          'gas': 1000000,
          'gasPrice': Web3.toWei(100, 'gwei'),
          'chainId': ch_id,
          'value': amount,
          'nonce': nonce
          }
    signed = ADMIN_ACC.signTransaction(tx)
    tx = node.eth.sendRawTransaction(signed.rawTransaction)
    rec = node.eth.waitForTransactionReceipt(tx)
    return rec


def deploy(abi, bytecode):
    # a=solc.compile_files([name])
    # a = a[list(a.keys())[0]]
    # file= open(name, 'r')

    contract = sokol.eth.contract(abi=abi, bytecode=bytecode)
    # ADMIN_ACC.encrypt('')

    acc = sokol.eth.account.create()
    sokol.eth.defaultAccount = acc.address

    tx = contract.constructor().buildTransaction()
    # tx = {'value': 0, 'gas': 203383, 'gasPrice': 10000000000, 'chainId': 77, 'from': acc.address, 'to': b''}
    # tx['data'] = file.read()
    # file.close()
    send_money(acc.address, tx['gas'] * tx['gasPrice'])
    tx['nonce'] = sokol.eth.getTransactionCount(acc.address)
    signed = acc.signTransaction(tx)
    tx_hash = sokol.eth.sendRawTransaction(signed.rawTransaction)
    tx_receipt = sokol.eth.waitForTransactionReceipt(tx_hash)
    return tx_receipt.contractAddress


def deploy_main(abi, bytecode, numbers, addresses):
    # a=solc.compile_files([name])
    # a = a[list(a.keys())[0]]
    # file= open(name, 'r')

    contract = sokol.eth.contract(abi=abi, bytecode=bytecode)
    # ADMIN_ACC.encrypt('')

    acc = sokol.eth.account.create()
    sokol.eth.defaultAccount = acc.address

    tx = contract.constructor(addresses, numbers).buildTransaction()
    # tx = {'value': 0, 'gas': 203383, 'gasPrice': 10000000000, 'chainId': 77, 'from': acc.address, 'to': b''}
    # tx['data'] = file.read()
    # file.close()
    send_money(acc.address, tx['gas'] * tx['gasPrice'])
    tx['nonce'] = sokol.eth.getTransactionCount(acc.address)
    signed = acc.signTransaction(tx)
    tx_hash = sokol.eth.sendRawTransaction(signed.rawTransaction)
    tx_receipt = sokol.eth.waitForTransactionReceipt(tx_hash)
    return tx_receipt.contractAddress


def deploy_stubs():
    deployed = []
    for i in range(5):
        file = open('./contract/abi_0_' + str(i) + '.txt', 'r')
        abi = file.read()
        # literal_eval(
        file.close()
        file = open('./contract/bin_0_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        deployed.append(deploy(abi, binary))
        deployed.append(deploy(abi, binary))

    return deployed


contracts = {}
contracts_addr = []


def deploy_main_1():
    global contracts_addr

    for i in range(5):
        stubs = deploy_stubs()
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, stubs))
        contracts[contracts_addr[-1]] = [stubs, numbers]


def deploy_main_2(cd):
    global contracts_addr

    for i in range(5):
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, cd))
        contracts[contracts_addr[-1]] = [cd, numbers]


def deploy_main_3(cd):
    global contracts_addr

    for i in range(5):
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, cd))
        contracts[contracts_addr[-1]] = [cd, numbers]


def deploy_main_4(cd):
    global contracts_addr

    for i in range(5):
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, cd))
        contracts[contracts_addr[-1]] = [cd, numbers]


def deploy_main_5(cd):
    global contracts_addr

    for i in range(5):
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, cd))
        contracts[contracts_addr[-1]] = [cd, numbers]


def deploy_main_6(cd):
    global contracts_addr

    for i in range(5):
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, cd))
        contracts[contracts_addr[-1]] = [cd, numbers]


def deploy_main_7(cd):
    global contracts_addr

    for i in range(5):
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, cd))
        contracts[contracts_addr[-1]] = [cd, numbers]


def deploy_main_8(cd):
    global contracts_addr

    for i in range(5):
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, cd))
        contracts[contracts_addr[-1]] = [cd, numbers]


def deploy_main_9(cd):
    global contracts_addr

    for i in range(1):
        numbers = [random.randint(0, 1024) for i in range(10)]
        file = open('./contract/abi_1_' + str(i) + '.txt', 'r')
        abi = file.read()
        file.close()
        file = open('./contract/bin_1_' + str(i) + '.txt', 'r')
        binary = file.read()
        file.close()
        contracts_addr.append(deploy_main(abi, binary, numbers, cd))
        contracts[contracts_addr[-1]] = [cd, numbers]


def contract(addr, x, y, depth, timestamp, addrrss):
    addrrss.append(addr)
    if depth > 9:
        return addrrss
    try:
        addrs, numbers = contracts[addr]
        a = addrs[(numbers[x % len(numbers)] * y) % len(addrs)]
        return contract(a, x, y, depth + 1, timestamp, addrrss)
    except:
        return addrrss


def for_parametr(num):
    l = hex(num)[2:]
    return (256 * 2 - len(l)) * '0' + l


fff = 'retrieve__1(uint256,uint256,uint256)'
answ = {}


def make_data_set():
    global contracts_addr
    global answ
    contracts_addr = []

    deploy_main_1()
    print('-')
    deploy_main_1()
    print('-')

    cd = contracts_addr
    contracts_addr = []
    deploy_main_2(cd)
    print('-')
    deploy_main_2(cd)
    print('-')

    cd = contracts_addr
    contracts_addr = []
    deploy_main_3(cd)
    print('-')
    deploy_main_3(cd)

    cd = contracts_addr
    contracts_addr = []
    print('-')
    deploy_main_4(cd)
    print('-')
    deploy_main_4(cd)

    cd = contracts_addr
    contracts_addr = []
    print('-')
    deploy_main_5(cd)
    print('-')
    deploy_main_5(cd)
    print('-')

    cd = contracts_addr
    contracts_addr = []
    deploy_main_6(cd)
    print('-')
    deploy_main_6(cd)
    print('-')

    cd = contracts_addr
    contracts_addr = []
    deploy_main_7(cd)
    print('-')
    deploy_main_7(cd)
    print('-')

    cd = contracts_addr
    contracts_addr = []
    deploy_main_8(cd)
    print('-')
    deploy_main_8(cd)
    print('-')

    cd = contracts_addr
    contracts_addr = []
    deploy_main_9(cd)
    print('-')
    # deploy_main_9(cd)
    print('-')

    for i in contracts_addr:
        x = random.randint(0, 1024)
        y = random.randint(0, 1024)
        depth = 1
        addr = i
        block_numb = random.randint(0, sokol.eth.block_number())
        timestamp = sokol.eth.getBlock(block_numb).timestamp
        fun = sokol.sha3(fff.encode()).hex()[:10] + for_parametr(x) + for_parametr(y) + for_parametr(depth)
        curl = """curl http://sokol.poa.network -X POST --data '{"jsonrpc":"2.0", "method":"eth_call", "params":[{"to": \"""" + addr + """", "data":\"""" + fun + """"}], "id":1}' -H 'content-type: application/json'"""
        answ[curl] = []

    data = answ
    new_data = {}
    global database
    global addresses
    for f in data:
        database = defaultdict(dict)
        addresses = set()
        chain = OnlineChain.from_genesis(
            AtomicDB(),
            {
                'parent_hash': constants.GENESIS_PARENT_HASH,
                'uncles_hash': constants.EMPTY_UNCLE_HASH,
                'coinbase': constants.ZERO_ADDRESS,
                'transaction_root': constants.BLANK_ROOT_HASH,
                'receipt_root': constants.BLANK_ROOT_HASH,
                'difficulty': constants.GENESIS_DIFFICULTY,
                'block_number': 18649394,
                'gas_limit': 1000000000,
                'extra_data': constants.GENESIS_EXTRA_DATA,
                'nonce': constants.GENESIS_NONCE
            }
        )
        vm = chain.get_vm()
        state = vm.state

        contract = decode_hex(eval(f[46:-37])['params'][0]['to'])
        data = decode_hex(eval(f[46:-37])['params'][0]['data'])

        private_key = keys.PrivateKey(w3.eth.account.create().privateKey)

        tx = vm.create_unsigned_transaction(
            nonce=0,
            gas_price=0,
            gas=900000000,
            to=contract,
            value=0,
            data=data
        )
        signed_tx = tx.as_signed_transaction(private_key)
        chain.apply_transaction(signed_tx)

        addresses = sorted(addresses)
        addresses = [w3.toChecksumAddress(encode_hex(address)) for address in addresses]

        new_data[f] = addresses
    file = open('template.py', 'r')
    templ = file.read()
    file.close()

    file = open('stepik.py', 'w')
    file.write('data = ' + str(new_data) + '\n')
    file.write(templ)
    file.close()


make_data_set()
